/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

//Temporary
import Reports from'./_5_Reports';

AppRegistry.registerComponent(appName, () => App);
