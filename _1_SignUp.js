/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React,{ useState } from 'react';
 import {
   StyleSheet,
   View,
   Text,
   StatusBar,
   Image,
   TextInput,
   TouchableOpacity,
 } from 'react-native';

 import CheckBox from '@react-native-community/checkbox'
 
 const SignUp=({navigation})=>
 {
   const [email, setEmail] = useState("");
   const [password, setPassword] = useState("");
   const [toggleCheckBox, setToggleCheckBox] = useState(false);
  
   return (
       <View style={styles.box1_CenteringPage}>
 
         <StatusBar barStyle='dark-content' hidden={false} backgroundColor='#ffb3b3' translucent={true} />
 
         <View style={styles.box2_Centered}>
           
           <View style={styles.box3_Stretched}>
 
             <Text style={styles.box3Text}>
 
               <Text style={{fontWeight:'bold',fontSize:25,color:'black'}}>
                 Sign Up
               </Text> 
 
               {'\n'}
               {'\n'}
               Hi there nice to meet you again :D
 
             </Text>
 
           </View>
 
           <View style={styles.box3_Stretched}>
 
             <Text style={styles.box3Text,{color:'#ff6666',fontWeight:'bold'}}>
               Email
             </Text>
 
             <TextInput style={styles.box3Input}
               onChangeText={(email) => setEmail(email)}
             />
 
             <View style={styles.line}/>
 
           </View>
 
           <View style={styles.box3_Stretched}>
 
             <Text style={styles.box3Text,{color:'#ff6666',fontWeight:'bold'}}>
               Password
             </Text>
           
             <TextInput style={styles.box3Input}
               secureTextEntry={true}
               onChangeText={(password) => setPassword(password)}
             />
 
             <View style={styles.line}/>
             
           </View>

          <View style={styles.box3_Stretched}>
 
            <Text style={styles.box3Text,{color:'#ff6666',fontWeight:'bold'}}>
              Confirm Password
            </Text>
           
            <TextInput style={styles.box3Input}
               secureTextEntry={true}
               onChangeText={(password) => setPassword(password)}
            />
 
            <View style={styles.line}/>
             
          </View>
 
          <View style={styles.box3_Row}>

            <CheckBox
            style={{alignSelf:'center'}}
    disabled={false}
    value={toggleCheckBox}
    onValueChange={(newValue) => setToggleCheckBox        (newValue)}
            />

            <Text style={styles.box3Text,              {alignSelf:'center', lineHeight:60,color:'#737373'}}>
              I agree to the 
            </Text>

          </View>

          <View style={styles.box3_Stretched}>
            <TouchableOpacity style={styles.loginButton} onPressOut={()=>navigation.navigate('SignIn')}>
              <Text style={styles.buttonText}>
                Continue
              </Text>
            </TouchableOpacity>
          </View>
 
          <View style={styles.box3_Row}>
 
            <Text style={styles.box3Text}>
              Have an account? 
            </Text>
 
            <TouchableOpacity style={{marginLeft:3,marginRight:3}}>
              <Text style={styles.box3Text,{fontSize:17,fontWeight:'bold',color:'#ff6666'}}>
                Sign In
              </Text>
            </TouchableOpacity>
 
          </View>
 
        </View>
 
     </View>
   );
 
 }
 
 const styles=StyleSheet.create(
   {
     box1_CenteringPage:
     {
       alignItems:'center',
       width:'100%'
     },
 
     box2_Centered:
     {
       alignItems:'center',
       width:'100%',
       marginTop:'20%',
     },
 
     box2Image:
     {
       height:200,
       resizeMode:'contain',
     },
 
     box3_Stretched:
     {
       alignItems:'stretch',
       width:'80%',
       marginTop:20,
     },
 
     box3Text:
     {
       marginTop:1,
       marginBottom:1,
       color:'#737373'
     },
 
     box3Input:
     {
       marginTop:1,
       marginBottom:1,
       fontSize:14,
       paddingTop:5,
       paddingBottom:0,
     },
 
     box3_Row:
     {
       flexDirection:'row',
       width:'75%',
       justifyContent:'center',
       marginBottom:20,
       lineHeight:20,
       
     },
 
     box3Image:
     {
       height:40,
       width:150,
       borderRadius:8,
     },
 
     line:
     {
       backgroundColor: '#4d4d4d',
       width:'100%',
       height:1,
       marginTop:1,
       marginBottom:1,
     },
 
     loginButton: 
     {
       width:'100%',
       height:40,
       borderRadius: 8,
       marginTop: 5,
       backgroundColor: "#ff6666",
       alignSelf:'center',
       justifyContent:'center'
     },
 
     buttonText:
     {
       color:'#ffffff',
       fontSize:20,
       alignSelf:'center',
     }
   }
 )
 
 export default SignUp;
